FROM registry.gitlab.com/rdenham/regionalwoody:dev
COPY . /opt/
RUN cd /opt/ && \
    Rscript -e "install.packages('renv')" && \
    Rscript -e "options(renv.consent = TRUE);renv::restore()" && \
    R CMD INSTALL .
