# Notes on this Repository

This project is public, and uses gitlab's CI/CD to build both a package and a docker image with the package in it.

I'm putting a couple of notes here on how I set it up, just in case its useful in future.

## Docker

Early attempts at building and testing the package worked ok, but they relied on pulling an image and 
installing packages into the image before running the test. Here is my early example of the build
script:

```yaml
build:
  stage: build
  script:
    - apt update -y
    - apt install -y libcurl4-openssl-dev libssl-dev libxml2-dev libfontconfig1-dev gettext-base moreutils libgdal-dev libudunits2-dev
    - export PACKAGE_VERSION=${PACKAGE_VERSION}
    - export PACKAGE=${PACKAGE}
    - envsubst '${PACKAGE_VERSION}' < DESCRIPTION | sponge DESCRIPTION
    - cat DESCRIPTION
    - R -e "install.packages(c('dplyr', 'survival', 'knitr', 'rmarkdown', 'terra'), repos = c(CRAN = 'https://cran.csiro.au/'))"
    - R -e "install.packages(c('sf', 'tidyr', 'ggplot2'), repos = c(CRAN = 'https://cran.csiro.au/'))"
    - R CMD build . --no-build-vignettes --no-manual
    - R CMD check *tar.gz --no-build-vignettes --no-manual
```

This was a little bit frustrating, because it took *ages* to install all the dependencies before I could do the check, so
was a bit too slow for REPL.

So, I then thought that I could start from a docker image that has the libraries pre-installed. I created a project
using the excellent R package `renv` just to get an `renv.lock` file, then built a docker image via the `.gitlab-ci.yml` file.

The Docker file looked like

```docker
FROM rocker/rstudio:4.0.0
COPY . /opt/
RUN apt update -y &&  \
    apt install -y libcurl4-openssl-dev libssl-dev libxml2-dev \
    libfontconfig1-dev gettext-base moreutils libgdal-dev \
    libudunits2-dev && \
    cd /opt/ && \
    Rscript -e "install.packages('renv')"
    Rscript -e "options(renv.consent = TRUE);renv::restore()"
```

And I created a dev docker image by:

```yaml
build_kaniko_command:
    stage: build
    variables:
      # To push to a specific docker tag other than latest(the default), amend the --destination parameter, e.g. --destination $CI_REGISTRY_IMAGE:$CI_BUILD_REF_NAME
      # See https://docs.gitlab.com/ee/ci/variables/predefined_variables.html#variables-reference for available variables
      IMAGE_DESTINATION: ${CI_REGISTRY_IMAGE}:dev
    image:
        name: gcr.io/kaniko-project/executor:debug
        entrypoint: [""]
    script:
        # Prepare Kaniko configuration file
        - echo "{\"auths\":{\"$CI_REGISTRY\":{\"username\":\"$CI_REGISTRY_USER\",\"password\":\"$CI_REGISTRY_PASSWORD\"}}}" > /kaniko/.docker/config.json
        # hard to use colons in yaml so I do it in 2 steps
        - echo -n 'Version:' >> DESCRIPTION
        - echo " ${PACKAGE_VERSION}" >> DESCRIPTION
        - sed -r '/^\s*$/d' DESCRIPTION > DESCRIPTION.tmp
        - mv DESCRIPTION.tmp DESCRIPTION
        - /kaniko/executor --context $CI_PROJECT_DIR --dockerfile $CI_PROJECT_DIR/Dockerfile --destination $IMAGE_DESTINATION
        # Print the full registry path of the pushed image
        - echo "Image pushed successfully to ${IMAGE_DESTINATION}"
```

This now gives me a `dev` docker image I can use as a base for my test, before installing it into that base. That speeds things up considerably.

What I plan to do is to make that a little more automated, so for example, if I update the `renv.lock` file, then it should update the
`dev` docker image.

## Using gitlab tags in the R package version

I also struggled a little bit with this. In the end, I decided that I would omit the `Version:` field from the DESCRIPTION 
file, and just append it via the `.gitlab-ci.yml` file, like:

```yaml
default:
  before_script:
    - DEFAULT_DEV_TAG=$(git describe --tags --abbrev=0 2>/dev/null)|| DEFAULT_DEV_TAG='0.0.0'
    - PACKAGE_VERSION=${CI_COMMIT_TAG:-$DEFAULT_DEV_TAG}
    - echo -n 'Version:' >> DESCRIPTION
    - echo " ${PACKAGE_VERSION}" >> DESCRIPTION
    - sed -r '/^\s*$/d' DESCRIPTION > DESCRIPTION.tmp
    - mv DESCRIPTION.tmp DESCRIPTION
```
I also had some trouble getting the yaml file to work, since the string `Version:` was misinterpreted
as a yaml directive. I got around this by doing a couple of steps.

This works pretty well for pipeline building, but it probably means that you can't install it
by `devtools::install_git` anymore. Which breaks the README.

So instead I added a version tag to the DESCRIPTION, but stripping it out in the pipeline to 
update it. The only thing now is that I need to keep the default version in the DESCRIPTION up to date.


