
<!-- README.md is generated from README.Rmd. Please edit that file -->

# regionalwoody

<!-- badges: start -->

<!-- badges: end -->

This package aims to demonstrate how you can make use of publicly
available spatia data to investigate woody vegetation characteristics.

## Installation

You can install this package from this gitlab repository using
`devtools`, for example

``` r
devtools::install_git(
  "https://gitlab.com/rdenham/regionalwoody.git"
)
```

## Online Data

In this package we show how to use data from
[auscover](http://www.auscover.org.au/). For example, there are regular
landsat surface reflectance images for Australian states at
<http://qld.auscover.org.au/public/data/landsat/surface_reflectance/>.
These tend to be large files, but you don’t need to download the
complete file to access the data in specific regions. Instead, you can
access the data for specific subsets by using a spatial layer.

## Example

Here we’ll use [google earth](https://earth.google.com) to help select
the area we are interested in. Browse to your example location, draw a
polygon, and add to a project or kml. When finished, download the kml to
your computer, then read it in. I chose a small area from [Mariala
National
Park](https://earth.google.com/earth/d/1I-HD2reUVB3GkYqmwGmuNuliyLxNNXVT?usp=sharing).
On the ‘more actions’ icon, export this to kml. You can open this in R.

``` r
library(terra)
#> terra version 1.3.4
library(regionalwoody)
#> 
#> Attaching package: 'regionalwoody'
#> The following object is masked from 'package:utils':
#> 
#>     vi
library(sf)
#> Linking to GEOS 3.8.0, GDAL 3.0.4, PROJ 6.3.1

mariala.sp <- read_sf("readme_data/Mariala National Park Example.kml")
```

The landsat data we are going to use is in Australian Albers
[EPSG:3577](https://spatialreference.org/ref/epsg/3577/), so it’s
convenient to reproject your data

``` r
mariala.sp <- st_transform(mariala.sp, 3577)
```

Using the [`terra`](https://github.com/rspatial/terra) package means we
can get data just for this section.

Lets get data for the winter season of 2020. Noote, this won’t be fast,
still a fair bit of work going on to access the data, but we’re not
downloading the whole thing which is 23Gig in
size.

``` r
url = "/vsicurl/http://qld.auscover.org.au/public/data/landsat/surface_reflectance/qld/l8olre_qld_m202006202008_dbia2.tif"
therast <- terra::rast(url)
croprast = terra::crop(therast, mariala.sp)
ref = croprast * 0.0001
ref[croprast == 32767] <- NA
```

This is a tiny subset, just 7 x 7 pixels with 6 bands (layers).

From this, we can calculate foliage projective cover. There are a number
of methods this can be done. First, we do the simple multiple linear
regression based on Armston et. al. but updated with addtional field
data and omitting a vapour pressure deficit layer. Since this
reflectance image is from landsat 8, a little fudge to calibrate it to
landsat 7 ETM+ is done first.

``` r
refetm <- fudgeOLItoETM_sfcRef(ref)
fpc <- fpcsq(refetm)
```
